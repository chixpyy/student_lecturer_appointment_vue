import axios from 'axios';

const API_URL = 'http://127.0.0.1:8000/api/';  //'http://10.175.3.28:90/api/';

class AuthService {
  login(user) {
    return axios
      .post(API_URL + 'signin', {
        email: user.email,
        password: user.password
      })
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem('user', JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.removeItem('user');
  }

  register(user) {
    return axios.post(API_URL + 'signup', {
      username: user.username,
      email: user.email,
      password: user.password
      // reg_number: user.reg_number
    });
  }
}

export default new AuthService();
