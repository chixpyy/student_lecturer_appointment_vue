import Profile from "./profile";

export default class User {
  constructor(username, email, password, reg_number, firstName,lastName,phoneNumber,gender) {
    this.username = username;
    this.email = email;
    this.password = password;
    // profile:new Profile(this.reg_number,this.firstName,this)

    this.reg_number = reg_number;
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.gender = gender;
  }
}
