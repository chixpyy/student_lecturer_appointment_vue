export default class Profile {
    constructor(reg_number, firstName,lastName,phoneNumber,gender) {
      this.reg_number = reg_number;
      this.firstName = firstName;
      this.lastName = lastName;
      this.phoneNumber = phoneNumber;
      this.gender = gender;
    }
  }